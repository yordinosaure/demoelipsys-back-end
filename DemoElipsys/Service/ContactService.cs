﻿using DemoElipsys.Models;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace DemoElipsys.Services
{
    public class ContactService
    {
        private readonly IMongoCollection<Contact> _contacts;

        public ContactService(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("ContactStoreDb"));
            var database = client.GetDatabase("DemoElipsys");

            _contacts = database.GetCollection<Contact>("Contacts");
        }

        public List<Contact> Get() =>
            _contacts.Find(contact => true).ToList();

        public Contact Get(string id) =>
            _contacts.Find<Contact>(book => book.Id == id).FirstOrDefault();

        public Contact Create(Contact contact)
        {
            _contacts.InsertOne(contact);
            return contact;
        }

        public void Update(string id, Contact contactIn) =>
            _contacts.ReplaceOne(contact => contact.Id == id, contactIn);

        public void Remove(Contact contactIn) =>
            _contacts.DeleteOne(contact => contact.Id == contactIn.Id);

        public void Remove(string id) =>
            _contacts.DeleteOne(contact => contact.Id == id);
    }
}