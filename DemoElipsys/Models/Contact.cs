﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DemoElipsys.Models
{
    public class Contact
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("FirstName")]
        public string FirstName { get; set; }

        [BsonElement("LastName")]
        public string LastName { get; set; }

        [BsonElement("Address")]
        public string Address { get; set; }

        [BsonElement("Number")]
        public string Number { get; set; }

        [BsonElement("ZipCode")]
        public string ZipCode { get; set; }

        [BsonElement("City")]
        public string City { get; set; }

        [BsonElement("AvatarUrl")]
        public string AvatarUrl { get; set; }

        [BsonElement("PhoneNumber")]
        public string PhoneNumber { get; set; }

        [BsonElement("Email")]
        public string Email { get; set; }
    }
}
